<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PagesController extends Controller {

	function about(){
        $people = [
          'Bear', 'Jackie', 'Pau'
        ];

//        $people = [];

        return view('pages.about', compact('people'));
    }

    function contact(){
        return view('pages.contact');
    }

}

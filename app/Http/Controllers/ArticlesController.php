<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Article;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Http\Requests\Request;
use App\Http\Requests\ArticleRequest;
use Illuminate\Support\Facades\Auth;

class ArticlesController extends Controller {

    /*
     * Will redirect user to login page if not authenticated.
     */

    public function __construct(){
        $this->middleware('auth');
    }

	public function index(){
        $articles = Article::latest('published_at')->published()->get();

        return view('articles.index', compact('articles'));
    }

    public function show(Article $article){
        return view('articles.show', compact('article'));
    }

    public function create(){
        return view('articles.create');
    }

    public function store(ArticleRequest $request){
        Auth::user()->articles()->create($request->all());

        \Session::flash('flash_message', 'Your article has been created');

        return redirect('articles');
    }

    public function edit(Article $article){
        return view('articles.edit', compact('article'));
    }

    public function update(Article $article, ArticleRequest $request){
        $article->update($request->all());

        return redirect('articles');
    }
}

@extends('app')

@section('content')

<h1>About Me</h1>

@if (count($people))

    <h3>People I Like</h3>
    <ul>
        @foreach($people as $person)
        <li>{{$person}}</li>
        @endforeach
    </ul>

@endif

<p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed nibh ante. Vivamus vulputate nisi at magna condimentum lobortis. Duis tempor luctus magna id ullamcorper. Nullam auctor est sit amet faucibus porta. Fusce suscipit, tellus ut lobortis tempor, nibh erat sollicitudin felis, nec elementum lacus urna a nunc. Quisque molestie mattis sodales. Nunc sagittis nunc neque, at rhoncus nulla posuere ac. Aenean viverra purus metus, eget volutpat ipsum auctor eu. Vivamus blandit, erat in facilisis lobortis, elit velit faucibus elit, ac pellentesque velit libero mattis erat. Donec tempor non justo ac fringilla.
</p>

@stop

